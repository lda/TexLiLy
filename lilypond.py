template = r"""
{version}
\header {{
    tagline = ""
}}
{content}
"""

miditemplate = r"""
{version}
\header {{
    tagline = ""
}}
\score {{
    {content}
    \layout{{}}
    \midi{{}}
}}
"""


def render(user: str, source: str, png: bool = True, version: str = None, template: bool = False) -> str:
    import tempfile
    import subprocess
    import utils
    import os
    versionstr = ""
    if version is not None:
        versionstr = r'\version "{version}"'.format(version=version)

    fmt = template.format(content=source, version=versionstr) if template else source

    tmp = tempfile.NamedTemporaryFile(delete=False)
    tmp.write(bytes(fmt, encoding="utf8"))
    tmp.close()
    if png:
        ret = subprocess.run(["lilypond", "-d", "preview", "--png",
                             "-dresolution=500", tmp.name], cwd="/tmp", capture_output=True)
        if ret.returncode != 0:
            raise FileNotFoundError(ret.stderr.decode())

        return tmp.name + ".preview.png"

    subprocess.run(["lilypond", "-d", "preview",
                   "--svg", tmp.name], cwd="/tmp")
    return tmp.name + "preview.svg"

def play(user: str, source: str, version: str = None) -> str:
    import tempfile
    import subprocess
    import utils
    import os
    
    if "soundfont" not in utils.get_config():
        raise Exception("No soundfont specified.")


    versionstr = ""
    if version is not None:
        versionstr = r'\version "{version}"'.format(version=version)

    fmt = miditemplate.format(content=source, version=versionstr)

    tmp = tempfile.NamedTemporaryFile(delete=False)
    tmp.write(bytes(fmt, encoding="utf8"))
    tmp.close()

    ret = subprocess.run(["lilypond", tmp.name], cwd="/tmp")
    if ret.returncode != 0:
        raise FileNotFoundError(ret.stderr.decode())

    # Now, use FluidSynth
    ret = subprocess.run(["fluidsynth", "-nli", "-r", "48000", "-o", "synth.cpu-cores=2", "-T", "oga", "-F", tmp.name + ".ogg", utils.get_config()["soundfont"], tmp.name + ".midi"], cwd="/tmp")

    return tmp.name + ".ogg"
