from nio import RoomMessageText
import typing


class Router():
    def __init__(self, name: str):
        self.routes = {}
        self.helptxt = {}
        self.name = name

    def add_route(self, cmd: str, hlp: str, func: typing.Callable) -> None:
        if cmd not in self.routes:
            self.routes[cmd] = func
            self.helptxt[cmd] = hlp

    async def handle_command(self, command: str, room: str, event: RoomMessageText, args: list):
        import utils
        if command in self.routes.keys():
            await self.routes[command](self, utils.get_client(), room, event, args)
