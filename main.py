from nio import *

import asyncio
import utils

async def main() -> None:
    import callbacks
    import getpass
    import router
    import yaml


    # Load our config file.
    config = {}
    client_config = AsyncClientConfig(
        max_limit_exceeded=0,
        max_timeouts=0,
        store_sync_tokens=True,
        encryption_enabled=True
    )

    try:
        with open('texlily.yaml', 'r+') as conf:
            config = yaml.safe_load(conf)

            # Retrieve our configuration
            homeserver = str(config["homeserver"])
            user = str(config["user"])
            path = str(config["data"])
            client = AsyncClient(homeserver, config=client_config, store_path=path, user=user)
            if 'token' in config:
                token = str(config["token"])
                deviceid = str(config["did"])
            
                client.user_id = user
                client.access_token = token
                client.device_id = deviceid
            else:
                print("It seems you haven't setup a token.")
                print("It's okay, let me just set everything up for you.")
                print("")

                pwd = getpass.getpass("Password:")

                print(await client.login(pwd))

                config["token"] = client.access_token
                config["did"] = client.device_id

            conf.truncate(0)
            conf.seek(0)
            yaml.dump(config, conf, default_flow_style=False)
            

        utils.set_config(config)
        # if config file does not exist, quit
    except FileNotFoundError:
        print("No config file found.")
        print("Please create a file named `texlily.yaml` and read the README file.")
        quit()
    except TypeError:
        print("Invalid configuration file.")
        quit()


    client.load_store()
    if client.should_upload_keys:
        await client.keys_upload()

    # Add routes.
    callbacks.init_routes()

    # Register all of the callbacks
    utils.set_client(client)
    client.add_event_callback(callbacks.msg_cb, RoomMessageText)
    client.add_event_callback(callbacks.file_cb, RoomMessageFile)
    client.add_event_callback(callbacks.invite_cb, InviteEvent)
    # async def other_cb(room: MatrixRoom, event: Event):
    client.add_event_callback(callbacks.other_cb, Event)


    await client.sync_forever(timeout=10000, full_state=True)

asyncio.run(main())
