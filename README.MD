# TeXLily: A LaTeX and Lilypond bot for [matrix]

TeXlily is a TeX and Lilypond bot designed for [matrix]. It is made for
clients who does not support LaTeX, but also for people willing to share
sheet music to their friends using Lilypond.

## Dependencies

TeXily only has a few dependencies:

    - Python 3 (tested on 3.11)
        + `matrix-nio[e2e]`
        + `pyaml`
        + `imagesize`
    - Lilypond
    - Optionally, FluidSynth for lily!play.
    - A distribution of TeX for `latex` and `dvipng` programs.
        + You also need the `standalone` and `varwidth` packages, which you 
        can find on CTAN.

Python: `pip install -r requirements.txt`

Arch Linux: `sudo pacman -S lilypond texlive`

## Contributing

You can contribute directly using PRs, or you can just mail me patches to:
`lda@freetards.xyz` with a title starting with `PATCH FOR TEXLILY:`

To contribute, use PEP8 with 4 spaces.

## How to setup
To setup TeXLily, you need to create a file named `texlily.yaml`, and make a 
directory where encryption keys and user styles will be stored.

The texlily.yaml file should look like this:
```yaml
homeserver: "your homeserver's base URL"
user: "@foo:bar.fed"
data: "the encryption/style directory"
# If you have FluidSynth installed
soundfont: "/absolute/dir/to/soundfont.sf2"
```

Then, start TeXLily with `python3 main.py`. Then, enter your password, *et voilà !*.

## License

TeXlily's is under the AGPLv3. Check COPYING.AGPL for more information on the
license.
