from nio import *


def set_client(c: AsyncClient) -> None:
    global client
    client = c


def get_client() -> AsyncClient:
    return client


def set_config(c: dict) -> None:
    global config
    config = c


def get_config() -> dict:
    return config
async def send_msg(client: Client, room: str, msg: str):
    content = {"msgtype": "m.text", "body": msg}

    try:
        await client.room_send(room, message_type="m.room.message", content=content, ignore_unverified_devices=True)
    except OlmUnverifiedDeviceError as e:
        client.verify_device(e.device)

def create_user_dir(user: str) -> str:
    import hashlib
    import os

    # NOTE: I would have used the user directly, but the Spec allows MXIDs
    # with '/' and UNIX-based OSes will **not** like that.
    sha = hashlib.sha256(bytes(user, 'ascii')).hexdigest()

    path = os.path.join(get_config()["data"], sha)
    if not os.path.exists(path):
        os.makedirs(path)

    return os.path.abspath(path)


async def send_png(room: str, filename: str) -> None:
    import imagesize
    import os
    basename = os.path.basename(filename)
    w, h = imagesize.get(filename)

    file = open(filename, "r+b")
    size = os.path.getsize(filename)

    filename = os.path.basename(filename)
    resp, keys = await client.upload(file, content_type="image/png", filename=basename)

    if not isinstance(resp, UploadResponse):
        raise OSError("Couldn't upload file.")

    content = {
        "body": basename,
        "info": {
            "size": size,
            "mimetype": "image/png",
            "w": w, "h": h
        },
        "msgtype": "m.image",
        "url": resp.content_uri,
    }
    file.close()
    try:
        await client.room_send(room, message_type="m.room.message", content=content, ignore_unverified_devices=True)
    except OlmUnverifiedDeviceError as e:
        client.verify_device(e.device)
async def send_ogg(room: str, filename: str) -> None:
    import imagesize
    import os
    basename = os.path.basename(filename)

    file = open(filename, "r+b")
    size = os.path.getsize(filename)

    filename = os.path.basename(filename)
    resp, keys = await client.upload(file, content_type="audio/ogg", filename=basename)

    if not isinstance(resp, UploadResponse):
        raise OSError("Couldn't upload file.")

    content = {
        "body": basename,
        "info": {
            "size": size,
            "mimetype": "audio/ogg",
        },
        "msgtype": "m.audio",
        "url": resp.content_uri,
    }
    file.close()
    try:
        await client.room_send(room, message_type="m.room.message", content=content, ignore_unverified_devices=True)
    except OlmUnverifiedDeviceError as e:
        client.verify_device(e.device)

async def send_plain(room: str, cnt: str) -> None:
    import imagesize
    import tempfile
    import io

    b = bytes(cnt, 'utf8')
    size = len(b)

    resp, keys = await client.upload(io.BytesIO(b), content_type="text/plain")

    if not isinstance(resp, UploadResponse):
        raise OSError("Couldn't upload file.")

    content = {
        "body": "error.log",
        "info": {
            "size": size,
            "mimetype": "text/plain"
        },
        "msgtype": "m.file",
        "url": resp.content_uri,
    }
    try:
        await client.room_send(room, message_type="m.room.message", content=content, ignore_unverified_devices=True)
    except OlmUnverifiedDeviceError as e:
        client.verify_device(e.device)

def verify_user(client: AsyncClient, user: str) -> None:
        for did, olm in client.device_store[user].items():
            client.verify_device(olm)
