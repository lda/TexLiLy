from router import Router
from nio import *

async def route_help(r: Router, client: Client, room: str, event: RoomMessageText, args: list) -> None:
    import router
    import utils

    txt = "Commands for TeXLily:\n"

    for cmd in r.routes:
        txt += f"  {r.name}!{cmd}: {r.helptxt[cmd]}\n"

    await utils.send_msg(client, room, txt)


async def route_delete(router: Router, client: Client, room: str, event: RoomMessageText, args: list) -> None:
    if len(args) == 1: 
        import utils
        import os

        directory = utils.create_user_dir(event.sender)

        file = os.path.join(directory, args[0] + ".sty")

        try:
            os.remove(file)
            await utils.send_msg(client, room, "Deleted!")
        except FileNotFoundError:
            await utils.send_msg(client, room, f"Couldn't delete style {args[0]}")
            pass

async def route_lshow(router: Router, client: Client, room: str, event: RoomMessageText, args: list) -> None:
    import lilypond
    import utils
    if len(args) >= 1:
        try:
            version = None
            if len(args) == 2:
                version = args[1]

            file = lilypond.render("", args[0], version=version)
            await utils.send_png(room, file)
        except FileNotFoundError as e:
            await utils.send_plain(room, e.args[0])
        except Exception:
            # ???
            pass
async def route_play(router: Router, client: Client, room: str, event: RoomMessageText, args: list) -> None:
    import lilypond
    import utils
    if len(args) >= 1:
        try:
            print("Playing")
            version = None
            if len(args) == 2:
                version = args[1]

            file = lilypond.play("", args[0], version=version)
            await utils.send_ogg(room, file)
        except Exception as e:
            #" ???
            print(":(", e.args[0])
            pass
