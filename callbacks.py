from router import Router
from routes import *
from nio import *


latex_regex = r'((?:\$[^\$]+\$)|(?:\$\$[^\$]+\$\$)|(?:\\\[[^\]]+\\\]))'

tex_router = Router("tex")
lily_router = Router("lily")

def init_routes() -> None:
    tex_router.add_route("help", "This very help menu.", route_help)
    tex_router.add_route("delete", "Removes a TeX style file.", route_delete)

    lily_router.add_route("help", "This very help menu.", route_help)
    lily_router.add_route("show", "Shows an in-line LilyPond expression", route_lshow)
    lily_router.add_route("s", "Shows an in-line LilyPond expression", route_lshow)
    lily_router.add_route("p", "Plays an in-line LilyPond expression", route_play)
    lily_router.add_route("play", "Plays an in-line LilyPond expression", route_play)

# Our message callback. It should be passed through a router.
async def msg_cb(room: MatrixRoom, event: RoomMessageText) -> None:
    import latex
    import utils
    import shlex
    import re

    client = utils.get_client()
    if event.sender == client.user_id:
        return
    
    if event.body.startswith("tex!"):
        import router
        try:
            args = shlex.split(event.body[4:])
            if len(args) >= 1:
                await tex_router.handle_command(args[0], room.room_id, event, args[1:])
        except ValueError:
            await utils.send_msg(client, room.room_id, "Badly formatted command.")
        return

    if event.body.startswith("lily!"):
        import router
        try:
            args = shlex.split(event.body[5:])
            if len(args) >= 1:
                await lily_router.handle_command(args[0], room.room_id, event, args[1:])
        except ValueError:
            await utils.send_msg(client, room.room_id, "Badly formatted command.")
        return

    if event.body.startswith("ly!"):
        import router
        try:
            args = shlex.split(event.body[3:])
            if len(args) >= 1:
                await lily_router.handle_command(args[0], room.room_id, event, args[1:])
        except ValueError:
            await utils.send_msg(client, room.room_id, "Badly formatted command.")
        return

    if event.formatted_body is not None:
        import lilypond
        import parser

        code = parser.CodeParser()
        code.feed(event.formatted_body)
        code.close()

        for block in code.blocks:
            content = block['content']
            if block['lang'] == 'tex':
                try:
                    filename = latex.render(event.sender, content)
                    await utils.send_png(room.room_id, filename)
                except FileNotFoundError as e:
                    await utils.send_plain(room.room_id, e.args[0])
            if block['lang'] == 'ly':
                try:
                    filename = lilypond.render(event.sender, content, template=False)
                    await utils.send_png(room.room_id, filename)
                except FileNotFoundError as e:
                    await utils.send_plain(room.room_id, e.args[0])
        if len(code.blocks) > 0:
            return


    for tex in re.findall(latex_regex, "\n".join([n for n in event.body.split('\n') if not n.startswith('>')]), re.M):
        try:
            filename = latex.render(event.sender, tex)
            await utils.send_png(room.room_id, filename)
        except FileNotFoundError as e:
            await utils.send_plain(room.room_id, e.args[0])
        except OSError:
            content = {
                "msgtype": "m.text",
                "body": "???",
                "formatted_body": "???"
            }

            await client.room_send(room.room_id, message_type="m.room.message", content=content)

# Our file callback.
async def file_cb(room: MatrixRoom, event: RoomMessageFile) -> None:
    import utils
    import os

    url = event.url
    filename = event.body

    if filename.endswith(".sty"):
        # Download file and save it for user.
        client = utils.get_client()
        directory = utils.create_user_dir(event.sender)
        response = await client.download(url, filename)
        
        if not isinstance(response, DownloadResponse):
            return

        with open(os.path.join(directory, filename), 'wb') as f:
            f.write(response.body)

async def invite_cb(room: MatrixRoom, event: InviteEvent) -> None:
    import utils
    await utils.get_client().join(room_id=room.room_id)
async def other_cb(room: MatrixRoom, event: Event):
    if event.source['type'] == "m.room.encrypted":
        import utils
        client = utils.get_client()
        utils.verify_user(client, event.sender)
